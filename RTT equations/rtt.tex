%% ****** Start of file apstemplate.tex ****** %
%%
%%
%%   This file is part of the APS files in the REVTeX 4 distribution.
%%   Version 4.1r of REVTeX, August 2010
%%
%%
%%   Copyright (c) 2001, 2009, 2010 The American Physical Society.
%%
%%   See the REVTeX 4 README file for restrictions and more information.
%%
%
% This is a template for producing manuscripts for use with REVTEX 4.0
% Copy this file to another name and then work on that file.
% That way, you always have this original template file to use.
%
% Group addresses by affiliation; use superscriptaddress for long
% author lists, or if there are many overlapping affiliations.
% For Phys. Rev. appearance, change preprint to twocolumn.
% Choose pra, prb, prc, prd, pre, prl, prstab, prstper, or rmp for journal
%  Add 'draft' option to mark overfull boxes with black boxes
%  Add 'showpacs' option to make PACS codes appear
%  Add 'showkeys' option to make keywords appear
\documentclass[aps,preprint,prl,showpacs,superscriptaddress,amsmath,amssymb]{revtex4-1}
\bibliographystyle{apsrev4-1}
%\usepackage{mwe}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{float}
\usepackage{amssymb}
\usepackage{xcolor}
\graphicspath{{figures/}} %Setting the graphicspath
%\usepackage[version=4]{mhchem}
\usepackage{caption}
\usepackage{subfigure}
%\documentclass[aps,prl,preprint,superscriptaddress]{revtex4-1}
%\documentclass[aps,prl,reprint,groupedaddress]{revtex4-1}

% You should use BibTeX and apsrev.bst for references
% Choosing a journal automatically selects the correct APS
% BibTeX style file (bst file), so only uncomment the line
% below if necessary.
\bibliographystyle{apsrev4-1}

\begin{document}
The computational grid nodes can be divided into two parts: bulk ($\pmb x_{f}\in \pmb \Omega$) and boundary points ($\pmb x_{b}\in \partial \pmb \Omega_D$), where the discrete boundary is given by $\partial \pmb \Omega_D$, as in Fig.\ref{rtt}. For every bulk fluid grid node denoted by $\pmb x_f$, the LB equation is given by,
\begin{align}
	\begin{split}
		f_i( \pmb x_f, t + \Delta t) = & f_i(\pmb x_f-\pmb c_{i}\Delta t, t) 
		+ 2 \beta \left[ f_i^{eq}(\pmb x_f- \pmb c_{i}\Delta t,\rho, \pmb u, t) - f_i(\pmb x_f-\pmb c_{i}\Delta t, t) \right].
	\end{split}
	\label{rtt1}
\end{align}
\begin{figure}[h]
	\includegraphics[scale=0.5]{fig_5}% Here is how to import EPS art
	\caption{\label{rtt} 2d representation of a computational grid}
\end{figure}

Upon multiplying by discrete velocity $\pmb c_i$ on both sides and summing for all discrete velocities, we get,
\begin{align}
	\begin{split}
		\sum_i \pmb c_i f_i( \pmb x_f, t + \Delta t) = & \sum_i \pmb c_i f_i(\pmb x_f-\pmb c_{i}\Delta t, t) 
		+ 2 \beta \sum_i \pmb c_i \left[ f_i^{eq}(\pmb x_f- \pmb c_{i}\Delta t,\rho, \pmb u, t) - f_i(\pmb x_f-\pmb c_{i}\Delta t, t) \right].
	\end{split}
	\label{rtt2}
\end{align}
The construction of the equilibrium density function is such that for every bulk fluid node,
$\sum_i \pmb c_i f_i^{eq}(\pmb x_f,\rho, \pmb u, t) = \sum_i \pmb c_i f_i(\pmb x_f, t)$
, which reduces Eq.\ref{rtt2} to,
\begin{align}
	\begin{split}
		\sum_i \pmb c_i f_i( \pmb x_f, t + \Delta t) = & \sum_i \pmb c_i f_i(\pmb x_f-\pmb c_{i}\Delta t, t).
	\end{split}
	\label{rtt3}
\end{align}
Whereas, for every discrete boundary point denoted by $\pmb x_b$, one must be careful with populations coming from the solid wall. Summation over the discrete velocity set on boundary points, can be divided into two parts such that,
\begin{align}
	\begin{split}
		\sum_i \pmb c_i f_i( \pmb x_b, t + \Delta t) =  \sum_{i \>\> \ni \> c_i n_i \leq 0} \pmb c_i f_i(\pmb x_{b}-\pmb c_{i}\Delta t, t) 
		&+ \>\> 2 \beta \sum_{i \>\> \ni \> c_i n_i \leq 0} \pmb c_i \left[ f_i^{eq}(\pmb x_b- \pmb c_{i}\Delta t,\rho, \pmb u, t) - f_i(\pmb x_b-\pmb c_{i}\Delta t, t) \right]
		\\& +\\
		\sum_{j \>\> \ni\> c_j n_j> 0} \pmb c_j \> R_{jk} \> f_k(\pmb x_{b}, t)
		&+ \>\> 2 \beta \sum_{j \>\> \ni\> c_j n_j> 0} \pmb c_j R_{jk} \left[ f_k^{eq}(\pmb x_b,\rho, \pmb u, t) - f_k(\pmb x_b, t) \right],
	\end{split}
	\label{rtt4}
\end{align}
where, $n_i$ denotes the normal to the solid surface and $R_{ij}$ is the boundary condition tensor. 

A complete domain grid sum over $\pmb \Omega$ and $\partial \pmb \Omega_D$ can be carried out using Eq.\ref{rtt3}) and Eq.\ref{rtt4}) as follows:
\begin{align}
	\begin{split}
		\sum_{\pmb \Omega} \sum_i \pmb c_i f_i( \pmb x_f, t + \Delta t) + \sum_{\partial \pmb \Omega_D} \sum_i \pmb c_i f_i( \pmb x_b, t + \Delta t) 
		=  \sum_{\pmb \Omega} \sum_i \pmb c_i f_i(\pmb x_f-\pmb c_{i}\Delta t, t) 
		\\
		+   \sum_{\partial \pmb \Omega} \left[ \sum_{i \>\> \ni \> c_i n_i \leq 0} \pmb c_i f_i(\pmb x_{b}-\pmb c_{i}\Delta t, t) + \sum_{j \>\> \ni\> c_j n_j> 0} \pmb c_j \> R_{jk} \> f_k(\pmb x_{b}, t) \right] 
		\\
		+  2 \beta  \sum_{\partial \pmb \Omega} \left[ \sum_{i \>\> \ni \> c_i n_i \leq 0} \pmb c_i \left[ f_i^{eq}(\pmb x_b- \pmb c_{i}\Delta t,\rho, \pmb u, t) - f_i(\pmb x_b-\pmb c_{i}\Delta t, t) \right] \right]
		\\
		+  2 \beta  \sum_{\partial \pmb \Omega} \left[ \sum_{j \>\> \ni\> c_j n_j> 0} \pmb c_j R_{jk} \left[ f_k^{eq}(\pmb x_b,\rho, \pmb u, t) - f_k(\pmb x_b, t) \right] \right]
	\end{split}
	\label{rtt5}
\end{align} 
\begin{align}
	\begin{split}
		J(t)|_{\pmb \Omega + \partial \pmb \Omega_D} = \sum_{\pmb \Omega} \sum_i \pmb c_i f_i(\pmb x_{f}, t) + \sum_{\partial \pmb \Omega} \sum_i \pmb c_i f_i(\pmb x_{b}, t) 
	\end{split}
	\label{lbe3}
\end{align}
Post collision balance at the boundary nodes is given by
\begin{align}
	\begin{split}
		\sum_{\partial \pmb \Omega} \sum_i \pmb c_i f_i( \pmb x_{fb}, t + \Delta t) = & \sum_{\partial \pmb \Omega} \sum_i \pmb c_i \tilde{f}_i(\pmb x_{fb}-\pmb c_{i}\Delta t, t) 
	\end{split}
	\label{lbe3}
\end{align}
\end{document}

